#!/bin/bash

# PREFIX should be expanded.
# conda will automatically replace it during installation.
# Note that this isn't true for BUILD_PREFIX.
EPICS_BASE="$PREFIX/epics"
EPICS_HOST_ARCH=$(perl src/tools/EpicsHostArch.pl)

cat << EOF >> configure/CONFIG_SITE.local
INSTALL_LOCATION=${EPICS_BASE}
MSI=\$(EPICS_BASE)/bin/\$(EPICS_HOST_ARCH)/msi
EOF

if [[ "$EPICS_TARGET_ARCH" == "linux-ppc64e6500" ]]
then
  echo "CROSS_COMPILER_TARGET_ARCHS=$EPICS_TARGET_ARCH" >> configure/CONFIG_SITE
  # Force require driver.Makefile to only compile for CROSS_COMPILER_TARGET_ARCHS
  echo "EXCLUDE_ARCHS=linux-x86_64" >> configure/CONFIG_SITE
fi

# When cross-compiling for linux-ppc64e6500, we first have to compile for linux-x86_64
# In that case we can't activate both the ifc14xx-toolchain compilers and anaconda compilers
# in the same environment. We use the default gcc for the initial x86 compilation.
# When creating the package for linux-x86_64, we use anaconda compilers.
if [[ "$EPICS_TARGET_ARCH" != "linux-ppc64e6500" ]]
then
  cat << EOF >> configure/os/CONFIG_SITE.Common.linux-x86_64
# Set GNU_DIR to BUILD_PREFIX or PREFIX if not set (when not using conda-build)
# Allow to compile without conda-build by installing manually the compilers
# in a local conda environment
GNU_DIR = \$(or \$(BUILD_PREFIX),$PREFIX)
CMPLR_PREFIX = x86_64-conda_cos6-linux-gnu-
EOF
fi

# Add conda env libs and headers path for linux-86_64
cat << EOF >> configure/os/CONFIG_SITE.Common.linux-x86_64
# --disable-new-dtags is required to avoid LD_LIBRARY_PATH overrride RPATH settings
OP_SYS_LDFLAGS += -Wl,--disable-new-dtags -Wl,-rpath,${PREFIX}/lib -Wl,-rpath-link,${PREFIX}/lib -L${PREFIX}/lib -Wl,-rpath-link,${EPICS_BASE}/lib/linux-x86_64
OP_SYS_INCLUDES += -I${PREFIX}/include
EOF

cat << EOF >> configure/os/CONFIG_SITE.Common.darwin-x86
GNU_DIR = \$(or \$(BUILD_PREFIX),$PREFIX)
CMPLR_PREFIX = x86_64-apple-darwin13.4.0-
CMPLR_CLASS = clang
CC = x86_64-apple-darwin13.4.0-clang
CCC = x86_64-apple-darwin13.4.0-clang++

OP_SYS_CFLAGS += -isysroot \${CONDA_BUILD_SYSROOT} -mmacosx-version-min=\${MACOSX_DEPLOYMENT_TARGET}
OP_SYS_CXXFLAGS += -isysroot \${CONDA_BUILD_SYSROOT} -mmacosx-version-min=\${MACOSX_DEPLOYMENT_TARGET}
OP_SYS_LDFLAGS += -Wl,-rpath,${PREFIX}/lib -L${PREFIX}/lib
OP_SYS_INCLUDES += -I${PREFIX}/include
EOF

# Compile epics-base
make -j${CPU_COUNT}

# Create files to set/unset variables when running
# activate/deactivate

# Note that modifying the PATH in deactivate scripts require conda >= 4.7
mkdir -p $PREFIX/etc/conda/activate.d
cat <<EOF > $PREFIX/etc/conda/activate.d/epics-base_activate.sh
export EPICS_BASE="$EPICS_BASE"
# When cross-compiling for ppc64, the perl installed in the host environment will fail to run on the build host.
# This is why we fallback to /usr/bin/perl to set EPICS_HOST_ARCH
# EPICS_HOST_ARCH should be set to the build host arch, this is why it's done at activation time.
export EPICS_HOST_ARCH="\$(perl ${EPICS_BASE}/lib/perl/EpicsHostArch.pl 2> /dev/null || /usr/bin/perl ${EPICS_BASE}/lib/perl/EpicsHostArch.pl)"
export EPICS_BASE_HOST_BIN="${EPICS_BASE}/bin/\${EPICS_HOST_ARCH}"
export EPICS_BASE_VERSION="${PKG_VERSION}"
export PATH=\$EPICS_BASE_HOST_BIN:\$PATH
EOF

mkdir -p $PREFIX/etc/conda/deactivate.d
cat <<EOF > $PREFIX/etc/conda/deactivate.d/epics-base_deactivate.sh
unset EPICS_BASE
unset EPICS_HOST_ARCH
unset EPICS_BASE_VERSION
export PATH=\$(echo \$PATH | sed "s?\$EPICS_BASE_HOST_BIN:??")
unset EPICS_BASE_HOST_BIN
EOF
