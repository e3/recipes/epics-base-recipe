epics-base conda recipe
=======================

Home: https://github.com/epics-base/epics-base

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS Base - main core of Experimental Physics and Industrial Control System

# Building EPICS base locally with conda

To build and install EPICS base locally using conda (for example, for as-yet-unsupported
architecture or os), you will need to have [conda-build](https://docs.conda.io/projects/conda-build/en/stable/)
installed. This can be done via
```
conda create -n conda-build conda-build
conda activate conda-build
```
which will create and activate a new environment with `conda-build` inside it.

From within this repository, you can then run
```
conda build --output-folder path/to/local/package/registry recipe
```
where `path/to/local/package/registry` is the local package registry. You may need to
create this first by running
```
conda index path/to/local/package/registry
```

In order to then build any package which depends on `epics-base`, you would build that
package via
```
conda build -c path/to/local/package/registry recipe
```
together with listing `epics-base` in the host requirements of that package.
